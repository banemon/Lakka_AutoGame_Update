# Lakka_AutoGame_Update
# 自动升级包
# 默认在线升级
# 也可手动升级
# 操作：
# 1.卡刷官方lakka，或自动列表lakka
# 	官方版：http://www.lakka.tv/   自动列表lakka : 度盘，群文件
# 2.将amupdate.zip与update.sh 放到共享目录Update里
# 3.如果你有设置过db里的设置文件，请先备份/storage/.config/db
# 4.删除/storage/.config/db
# 5.进入ssh，执行 sh .update/update.sh
# 6.将会自动重启
# 说明：
# 1.增加了更多的设置参数，详情看/storage/.config/db目录内的cfg类文件
# 2.增加了更多的街机类游戏的数据，1000+
# 3.优化了升级包的支持
# 	升级包存到Update里，重启或主菜单--》退出Retroarch，将自动升级
# 4.开机logo，将会跟随：设置--》用户界面--》菜单--》主题颜色，的设定。
# 	比如选的是紫色，先退出Retroarch，下次启动时，logo就是紫色。
# 5.开机动画，也是跟随主题颜色，视频只有两个，所以被动使用的是默认
# 	开机动画支持，自定义，config.cfg里面设置，如果自定义视频无效，将使用主题颜色设定，如果主题颜色设定无效，将使用默认
# 6.游戏列表，支持开头编号，开头字母，开头以子目录命名，隐藏游戏名中的括号部分，config.cfg里有开关。
# 7.优化了主背景图，更鲜明。
# 8.增加了一个设置菜单，进入ssh，即进菜单，或手工执行 sh amconfig.sh
# 9.amconfig.sh增加了本地升级功能，自动列出.update目录里的所有zip包，不满足结构的zip包，将会有提示，但不限制强刷。
# 10.新增2个开机视频，符合跟随主题颜色。
# 11.自制的开机logo和视频可以按目录结构打包成zip，以手工升级方式刷入，再到config.cfg里设置定指定即可。
# 
# 最后：
# 	虽然目前已有4999+的街机游戏中文数据，但仍会有很多游戏不能匹配到中文名称，所以希望广大爱好者一同补齐。
# 	尽可能的完美这个插件，还有很多不能面面俱到的bug，使用中发现奇怪的现象，及时反馈。
# 	开机logo 和开机动画，只要把图、视频和对应的key文件放入指定的目录，就可以使用，当然你得有key。
# 	
# 	玩得愉快
