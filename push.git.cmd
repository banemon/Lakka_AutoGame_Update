@echo off
for /f %%i in ('git config user.name') do set pushuser=%%i
@echo on
git pull origin master
git add .
@echo off
for /f %%i in ('git diff --name-only HEAD') do call set pushfile=%%pushfile%% %%i
set pushdate=%DATE:~0,10%-%TIME:~0,8%
set pushinfo=%1%2%3%4%5%6%7%8%9
set pushmarker=%pushdate% %pushinfo%--^>%pushfile%
@echo on
git commit -m "%pushmarker%"
git push origin master
pause