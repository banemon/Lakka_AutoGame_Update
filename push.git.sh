pushuser=$(git config user.name)
pushdate="_$(date +%y/%m/%d-%H:%M:%S)"
#pushdate="_$(date +%y/%m/%d-week%w-%H:%M:%S)"
pushinfo="${@:+"_$@"}"

git pull origin master
git add .

pushfile="$(git diff --name-only HEAD|sed -n 's:\n:,:g')"
pushmarker="$pushdate ${pushinfo}-->$pushfile"

git commit -m "$pushmarker"
git push origin master