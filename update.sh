#!/bin/sh
# 定义自身路径
dir_self="$(dirname "$(realpath "$0")")"
if [ -z "$1" ] || [ ! -f "$1" ];then
	file_2="amupdate.zip"
	i="$dir_self/$file_2"
else
	i="$1"
fi
unset update_yes
if test -f "$i";then
	systemctl stop retroarch
	mount -o remount,rw /flash
	unzip -o "$i" -d / 
	if [ $? -eq 0 ];then
		rm "$i"
		update_yes=1
	else
		systemctl start retroarch
		update_yes=0
		msg_info="未知错误"
		mount -o remount,ro /flash
	fi
else
	update_yes=0 
	msg_info="没有升级文件"
fi

if test "$update_yes" = "1";then
	rm /storage/.update/update.sh /storage/amconfig.sh
	clear
	a=">";
	echo -e "\n\n升级成功，将在 15 秒后重启！\n\n"
	wt=15
	for i in $(seq $wt);do
		let per=wt-i
		echo -e "     重启中 [\c"
		for ii in $(seq $wt);do
			echo -e "_\c"
		done
		echo -e "]$per 秒  \r\c"
		echo -e "     重启中 [$a\r\c"
		a="$a>";
		sleep 1s;
	done
	reboot
else
	echo -e "\n升级失败，$msg_info : $file_2\n"
fi